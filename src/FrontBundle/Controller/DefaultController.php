<?php

namespace FrontBundle\Controller;

use CoreBundle\Entity\Attribute;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    public function generateAction(){
        $token = md5(uniqid());
        return $this->render(':CV:first.html.twig', array(
            'token'=>$token
        ));
    }

    public function ajaxAction(Request $request){
        $value = $request->get('value');
        $pk = $request->get('pk');
        $token = $request->get('token');

        $em = $this->getDoctrine()->getManager();
        $attribute = $em->getRepository('CoreBundle:Attribute')->findOneBy(array(
            'token'=>$token,
            'type'=>$pk
        ));

        if($attribute == null){
            $attribute = new Attribute();
            $attribute->setToken($token);
            $attribute->setType($pk);
            $attribute->setValue($value);
            $em->persist($attribute);
            $em->flush();
            return new Response('OK');
        }

        $attribute->setValue($value);
        $em->flush();
        return new Response('OK');
    }

    public function indexAction()
    {
        return $this->render('@Front/Default/index.html.twig');
    }

    public function createAction(Request $request)
    {
        $token = $request->get('id');
        $em = $this->getDoctrine()->getManager();
        $attributes = $em->getRepository('CoreBundle:Attribute')->findBy(array('token'=>$token));
        $count = 0;
        foreach ($attributes as $attribute){
            $count++;
        }
        if ( $count < 26){
            $response = new Response();
            $response->setStatusCode(201);
            $response->setContent('CV incomplet');
            return $response;
        } else {
            return new Response($this->generateUrl('front_pdf_donwload', array('token'=>$token)));
        }

    }

    public function downloadAction($token){
        $em = $this->getDoctrine()->getManager();

        $html = $this->renderView(':CV:pdf.html.twig', array(
            'name'=>$em->getRepository('CoreBundle:Attribute')->findOneBy(array('token'=>$token, 'type'=>'name'))->getValue(),
            'apropos'=>$em->getRepository('CoreBundle:Attribute')->findOneBy(array('token'=>$token, 'type'=>'apropos'))->getValue(),
            'university1'=>$em->getRepository('CoreBundle:Attribute')->findOneBy(array('token'=>$token, 'type'=>'university-1'))->getValue(),
            'year1'=>$em->getRepository('CoreBundle:Attribute')->findOneBy(array('token'=>$token, 'type'=>'date-1'))->getValue(),
            'grade1'=>$em->getRepository('CoreBundle:Attribute')->findOneBy(array('token'=>$token, 'type'=>'grade-1'))->getValue(),
            'parcours1'=>$em->getRepository('CoreBundle:Attribute')->findOneBy(array('token'=>$token, 'type'=>'parcours-1'))->getValue(),
            'university2'=>$em->getRepository('CoreBundle:Attribute')->findOneBy(array('token'=>$token, 'type'=>'university-2'))->getValue(),
            'year2'=>$em->getRepository('CoreBundle:Attribute')->findOneBy(array('token'=>$token, 'type'=>'date-2'))->getValue(),
            'grade2'=>$em->getRepository('CoreBundle:Attribute')->findOneBy(array('token'=>$token, 'type'=>'grade-2'))->getValue(),
            'parcours2'=>$em->getRepository('CoreBundle:Attribute')->findOneBy(array('token'=>$token, 'type'=>'parcours-2'))->getValue(),
            'phone'=>$em->getRepository('CoreBundle:Attribute')->findOneBy(array('token'=>$token, 'type'=>'phone'))->getValue(),
            'address'=>$em->getRepository('CoreBundle:Attribute')->findOneBy(array('token'=>$token, 'type'=>'address'))->getValue(),
            'url'=>$em->getRepository('CoreBundle:Attribute')->findOneBy(array('token'=>$token, 'type'=>'url'))->getValue(),
            'linkedin'=>$em->getRepository('CoreBundle:Attribute')->findOneBy(array('token'=>$token, 'type'=>'linkedin'))->getValue(),
            'github'=>$em->getRepository('CoreBundle:Attribute')->findOneBy(array('token'=>$token, 'type'=>'github'))->getValue(),
            'company1'=>$em->getRepository('CoreBundle:Attribute')->findOneBy(array('token'=>$token, 'type'=>'company-1'))->getValue(),
            'company2'=>$em->getRepository('CoreBundle:Attribute')->findOneBy(array('token'=>$token, 'type'=>'company-2'))->getValue(),
            'year3'=>$em->getRepository('CoreBundle:Attribute')->findOneBy(array('token'=>$token, 'type'=>'year-3'))->getValue(),
            'year4'=>$em->getRepository('CoreBundle:Attribute')->findOneBy(array('token'=>$token, 'type'=>'year-4'))->getValue(),
            'description1'=>$em->getRepository('CoreBundle:Attribute')->findOneBy(array('token'=>$token, 'type'=>'description-1'))->getValue(),
            'description3'=>$em->getRepository('CoreBundle:Attribute')->findOneBy(array('token'=>$token, 'type'=>'description-3'))->getValue(),
            'description4'=>$em->getRepository('CoreBundle:Attribute')->findOneBy(array('token'=>$token, 'type'=>'description-4'))->getValue(),
            'poste1'=>$em->getRepository('CoreBundle:Attribute')->findOneBy(array('token'=>$token, 'type'=>'poste-1'))->getValue(),
            'poste2'=>$em->getRepository('CoreBundle:Attribute')->findOneBy(array('token'=>$token, 'type'=>'poste-2'))->getValue(),
            'skill1'=>$em->getRepository('CoreBundle:Attribute')->findOneBy(array('token'=>$token, 'type'=>'skill-1'))->getValue(),
            'level1'=>$em->getRepository('CoreBundle:Attribute')->findOneBy(array('token'=>$token, 'type'=>'level-1'))->getValue(),
        ));

        return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
            200,
            array(
                'Content-Type'          => 'application/pdf',
                'Content-Disposition'   => 'attachment; filename="file.pdf"'
            )
        );
    }
}
